import {defineStore} from 'pinia'


export const useAppStore = defineStore('appStore', {
    state() {
        return {
            /**
             * Static data
             */
            genders: ['male', 'female'],
            educationalLevels: ['L1', 'L2'],
            educationalSpecialities: ['S1', 'S2'],
            courses: ['C1', 'C2'],

            /**
             * Dynamic form data
             */
            step: 1,
            isFormLoading: false,
            isFormValid: false,
            isFormSuccess: false,
            formData: {
                firstName: '',
                lastName: '',
                birthDate: '',
                gender: '',
                parentFullName: '',
                parentPhoneNumber: '',
                institutionName: '',
                educationalLevel: '',
                educationalSpeciality: '',
                courses: [],
            },
            errors: [],
        }
    },
    actions: {
        register() {
            this.isFormLoading = true;

            /**
             * todo: call firebase api to store the registration form here
             * example:
             *  firebaseIns
             *      .post(this.formData)
             *      .then(successCB)
             *      .catch(errorCB)
             */
        },
    }
});
