import { createRouter, createWebHistory } from "vue-router";
import RegistrationFormView from "../views/RegistrationFormView.vue";
import RegistrationSucessView from "../views/RegistrationSucessView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "register-form",
      component: RegistrationFormView,
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/AboutView.vue')
    // }
  ],
});

export default router;
