import {createVuetify} from "vuetify";
import * as directives from "vuetify/directives";
import * as components from "vuetify/components";
import * as labComponents from "vuetify/labs/components";

const appTheme = {
    dark: false,
    colors: {
        background: '#FFFFFF',
        surface: '#FFFFFF',
        primary: '#2128f5',
        'primary-darken-1': '#2128f5',
        error: '#f52222',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    }
}

const vuetify = createVuetify({
    directives,
    components: {
        ...components,
        ...labComponents,
    },
    theme: {
        defaultTheme: 'appTheme',
        themes: {
            appTheme,
        }
    }
});


export default vuetify;